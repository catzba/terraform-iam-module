#-----------------------------------------------------------------------------------------------------
# IAM
#-----------------------------------------------------------------------------------------------------

#REQUIRED: Creates IAM user 
resource "aws_iam_user" "iam_user" {
  name = var.iam_username
  path = "/system/"
}

#REQUIRED: Creates keys for IAM user
resource "aws_iam_access_key" "iam_user" {
  user = aws_iam_user.iam_user.name
}

#REQUIRED: Create group for IAM user
resource "aws_iam_group" "iam_user_group" {
  name = "${var.iam_username}-group"
  path = "/users/"
}

#REQUIRED: Add members to iam_user_group
resource "aws_iam_group_membership" "iam_user_group_membership" {
  name  = "${var.iam_username}-group-membership"

  users = [
    aws_iam_user.iam_user.name
  ]

  group = aws_iam_group.iam_user_group.name
}
